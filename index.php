<?php

require "bootstrap.php";
use Chatter\Models\Car; 
use Chatter\Models\Student; 
use Chatter\Models\User; 
use Chatter\Models\Product; 
//use Chatter\Middleware\Logging; 

$app = new \Slim\App();
//$app->add(new Logging());
//lines 8-16 is lesson 2


//-------------------------------   Products  ---------------------------------------------------------------------------------------
$app->get('/products', function($request, $response,$args){    
        $_product = new Product();
        $products = $_product->all();
        $payload=[];
        foreach($products as $pro){
                    $payload[$pro->id] = [
                        'id'=> $pro->id,
                        'name'=> $pro->name,
                        'price'=> $pro->price
                     ];
                 }
                 return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/products/{product_id}', function($request, $response,$args){    
                $_product = Product::find($args['product_id']);
                $payload=[];
                if($_product->id){
                        return $response->withStatus(200)->withJson(json_decode($_product))->withHeader('Access-Control-Allow-Origin', '*');
                }else{
                        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
                }  
                         
});

$app->post('/products', function($request, $response,$args){
        $name = $request->getParsedBodyParam('name','');
        $price = $request->getParsedBodyParam('price','');    
        $_product = new Product();
        $_product->name = $name;
        $_product->price = $price;
        $_product->save();
        if($_product->id){
            $payload = ['product_id'=>$_product->id];
             return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
         else{
             return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     });


$app->delete('/products/delete/{product_id}', function($request, $response,$args){
           $_product = Product::find($args['product_id']); 
            $_product->delete();
            if($_product->exist){
                 return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
             }
             else{
                 return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
                 
             }
});

//PUT product - update user from database
$app->put('/products/{product_id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); //get the parameters from the post
    $price = $request->getParsedBodyParam('price',''); //get the parameters from the post
   
    $_product = Product::find($args['product_id']); // search the user with the id from the url
    
    if($name){
         $_product->name = $name;
    }
    if ($price){
           $_product->price = $price;
     }
    
    if($_product->save()){ //the update succeed
        $payload = ['product_id'=>$_product->id, "result"=>"The product has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //success
    } else{
        return $response->withStatus(400); //error
    }
});


//-------------------------------   Users  ---------------------------------------------------------------------------------------
$app->get('/users', function($request, $response,$args){    
        $_user = new User();
        $users = $_user->all();
        $payload=[];
        foreach($users as $usr){
                    $payload[$usr->id] = [
                        'id'=> $usr->id,
                        'name'=> $usr->name,
                        'phone'=> $usr->phone
                     ];
                 }
                 return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/users/{user_id}', function($request, $response,$args){    
                $_user = User::find($args['user_id']);
                $payload=[];
                if($_user->id){
                        return $response->withStatus(200)->withJson(json_decode($_user))->withHeader('Access-Control-Allow-Origin', '*');
                }else{
                        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
                }  
                         
});

$app->post('/users', function($request, $response,$args){
        $name  = $request->getParsedBodyParam('name','');
        $phone = $request->getParsedBodyParam('phone','');    
        $_user = new User();
        $_user->name    =  $name;
        $_user->phone  =  $phone;
        $_user->save();
        if($_user->id){
            $payload = ['user_id'=>$_user->id];
             return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
         else{
             return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     });


$app->delete('/users/delete/{user_id}', function($request, $response,$args){
           $_user = User::find($args['user_id']); 
            $_user->delete();
            if($_user->exist){
                 return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
             }
             else{
                 return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
                 
             }
});

//PUT USER - update user from database
$app->put('/users/{user_id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); //get the parameters from the post
    $phone = $request->getParsedBodyParam('phone',''); //get the parameters from the post
   
    $_user = User::find($args['user_id']); // search the user with the id from the url
    
    if($name){
         $_user->name = $name;
    }
    if ($phone){
           $_user->phone = $phone;
     }
    
    if($_user->save()){ //the update succeed
        $payload = ['user_id'=>$_user->id, "result"=>"The user has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //success
    } else{
        return $response->withStatus(400); //error
    }
});


//Login without JWT
$app->post('/login', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
 $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});



//אבטחת מידע
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});


//-------------------------------   Student  ---------------------------------------------------------------------------------------
$app->get('/students', function($request, $response,$args){    
        $_student = new Student();
        $students = $_student->all();
        $payload=[];
        foreach($students as $stud){
                    $payload[$stud->id] = [
                        'id'=> $stud->id,
                        'name'=> $stud->name,
                        'faculty'=> $stud->faculty
                     ];
                 }
                 return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/students/{student_id}', function($request, $response,$args){    
                $_student = Student::find($args['student_id']);
                $payload=[];
                if($_student->id){
                        return $response->withStatus(200)->withJson(json_decode($_student))->withHeader('Access-Control-Allow-Origin', '*');
                }else{
                        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
                }  
                         
});

$app->post('/students', function($request, $response,$args){
        $name = $request->getParsedBodyParam('name','');
        $faculty = $request->getParsedBodyParam('faculty','');    
        $_student = new Student();
        $_student->name   =  $name;
        $_student->faculty  =  $faculty;
        $_student->save();
        if($_student->id){
            $payload = ['student_id'=>$_student->id];
             return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
         else{
             return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     });


$app->delete('/students/delete/{student_id}', function($request, $response,$args){
           $_student = Student::find($args['student_id']); 
            $_student->delete();
            if($_student->exist){
                 return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
             }
             else{
                 return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
                 
             }
});

//PUT USER - update user from database
$app->put('/students/{student_id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); //get the parameters from the post
    $faculty = $request->getParsedBodyParam('faculty',''); //get the parameters from the post
   
    $_student = Student::find($args['student_id']); // search the user with the id from the url
    if($name){
         $_student->name = $name;
    }
    if ($faculty){
           $_student->faculty = $faculty;
     }
    
    if($_student->save()){ //the update succeed
        $payload = ['student_id'=>$_student->id, "result"=>"The student has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //success
    } else{
        return $response->withStatus(400); //error
    }
});


/*Login without JWT
$app->post('/login', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
 $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});
*/


//-------------------------------   Cars  ---------------------------------------------------------------------------------------
$app->get('/cars', function($request, $response,$args){    
        $_car = new Car();
        $cars = $_car->all();
        $payload=[];
        foreach($cars as $cr){
                    $payload[$cr->id] = [
                        'id'=> $cr->id,
                        'number'=> $cr->number,
                        'model'=> $cr->model
                     ];
                 }
                 return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/cars/{car_id}', function($request, $response,$args){    
                $_car = Car::find($args['car_id']);
                $payload=[];
                if($_car->id){
                        return $response->withStatus(200)->withJson(json_decode($_car))->withHeader('Access-Control-Allow-Origin', '*');
                }else{
                        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
                }  
                         
});

$app->post('/cars', function($request, $response,$args){
        $number = $request->getParsedBodyParam('number','');
        $model = $request->getParsedBodyParam('model','');    
        $_car = new Car();
        $_car->number   =  $number;
        $_car->model  =  $model;
        $_car->save();
        if($_car->id){
            $payload = ['car_id'=>$_car->id];
             return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
         else{
             return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     });


$app->delete('/cars/delete/{car_id}', function($request, $response,$args){
           $_car = Student::find($args['car_id']); 
            $_car->delete();
            if($_car->exist){
                 return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
             }
             else{
                 return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
                 
             }
});

//PUT USER - update user from database
$app->put('/cars/{car_id}', function($request, $response,$args){
    $number = $request->getParsedBodyParam('number',''); //get the parameters from the post
    $model = $request->getParsedBodyParam('model',''); //get the parameters from the post
   
    $_car = Car::find($args['car_id']); // search the user with the id from the url
    if($number){
      $_car->number = $number; //delete the old param, and insert the new param
    }
    if($model){
      $_car->model = $model; //delete the old param, and insert the new param
    }
    
    if($_car->save()){ //the update succeed
        $payload = ['car_id'=>$_user->id, "result"=>"The car has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //success
    } else{
        return $response->withStatus(400); //error
    }
});

$app->post('/Users/bulk', function($request, $response,$args){
        $payload =$request->getParsedBody();
        User::insert($payload);
         return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
         }
     
     );
     
$app->add(function ($req, $res, $next) {
         $response = $next($req, $res);
         return $response
                 ->withHeader('Access-Control-Allow-Origin', '*')
                 ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                 ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
 });






$app->get('/hello/{name}', function($request, $response,$args){
    return $response->write('Hello '.$args['name']);
});
$app->get('/customer/{number}', function($request, $response,$args){
    return $response->write('customer '.$args['number']. ' welcome to JCE');
});
$app->get('/customer/{cnumber}/product/{pnumber}', function($request, $response,$args){
    return $response->write('customer '.$args['cnumber']. ' product '.$args['pnumber']);
});

//READ MESSAGE - get the message information from database
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

//CREATE MESSAGE - insert message to database
$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});

//DELETE MESSAGE - delete message from database
$app->delete('/message/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']); //find the message in database
    $message->delete(); //delete the message
    if($message->exists){ //check if the message already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //message delete succesfully
    }
});
$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    //die("message id is " . $_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});


//DELETE USER - delete user from database

/* delete bulk
$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    User::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});
if ($data = $stmt->fetchAll()) {
    print_r(json_encode($data));
}*/


/*$app->delete('/users/bulk', function($request, $response,$args){ 
    $payload = $request->getParsedBody();
    $count = User::count(); 
    $deleteUs = User::latest()->take($count)->skip(1000)->get();
    try{
    foreach($deleteUs as $deleteMe){
                $ids = $deleteMe->id;
            }
    
    User::destroy($ids);
        }
        catch ( Illuminate\Database\QueryException $e) {
            
            var_dump($e->errorInfo);
            
            }
});*/





$app->run();
